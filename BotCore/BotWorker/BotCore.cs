﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using WishGameListBot.Commands;
using WishGameListBot.Repositories;

namespace WishGameListBot.BotWorker
{
    public abstract class BotCore
    {
        internal ITelegramBotClient botClient;

        private protected static Dictionary<long, List<string>> _dict = new Dictionary<long, List<string>>();
        private protected static Dictionary<long, string> _dictState = new Dictionary<long, string>();

        private protected CommandsCreator _creator;
        private protected Configuration _config;

        private protected IRepository _repository;

        #region WebHook
        public void InitializeWebHook()
        {
            WebhookInfo curHook = null;
            try
            {
                curHook = botClient.GetWebhookInfoAsync().Result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            if (string.IsNullOrEmpty(curHook.Url))
            {
                string hook = _config.ExternalURL + "api/update";
                botClient.SetWebhookAsync(hook);
            }
        }
        public void RemoveWebHook()
        {
            botClient.DeleteWebhookAsync();
        }
        #endregion

        private CommandArgs CreateCommandArgs(long chatId, string message, string[] parameters = null)
        {
            var args = new CommandArgs
            {
                ChatId = chatId
            };
            args.Input = message;
            args.Parameters = parameters;
            return args;
        }

        protected internal async Task SendMessage(long chatId, string text)
        {
            await botClient.SendTextMessageAsync(
                chatId: chatId,
                text: text
            );
        }

        protected internal async Task SendMessage(long chatId, string text, InlineKeyboardMarkup keyboard)
        {
            await botClient.SendTextMessageAsync(
                chatId: chatId,
                text: text,
                replyMarkup: keyboard
            );
        }

        protected internal async Task SendMessage(long chatId, string text, ReplyKeyboardMarkup keyboard)
        {
            await botClient.SendTextMessageAsync(
                chatId: chatId,
                text: text,
                replyMarkup: keyboard
            );
        }

        internal void RegisterUser(long chatId)
        {
            _dict.TryAdd(chatId, new List<string>());
        }

        internal void RegisterState(long chatId, string state)
        {
            UnRegisterState(chatId);
            _dictState.Add(chatId, state);
        }

        internal void UnRegisterState(long chatId)
        {
            if (_dictState.ContainsKey(chatId))
            {
                _dictState.Remove(chatId);
            }
        }

        internal string GetAvailableStores()
        {
            return string.Join(',', _config.AvailableStore);
        }

        internal string GetState(long chatId)
        {
            if (_dictState.ContainsKey(chatId))
                return _dictState[chatId];
            return null;
        }

        private async Task ParseCommand(long chatId, string cmd)
        {
            string command = cmd;
            string[] parameters = null;

            if (cmd.StartsWith('/')) // for stateless commands
            {
                var raw = cmd.Split();
                command = raw.FirstOrDefault();
                parameters = raw.Skip(1).ToArray();
            }

            var executive = _creator.Create(command);
            if (executive != null)
            {
                var args = CreateCommandArgs(chatId, command, parameters);
                await executive.ProcessAsync(args, this);
            }
            else
            {
                var stateCmd = GetState(chatId);
                if (stateCmd != null)
                {
                    executive = _creator.Create(stateCmd);
                    var args = CreateCommandArgs(chatId, stateCmd, cmd.Split());
                    await executive.ProcessAsync(args, this);
                }
            }
        }

        #region TypesHandlers

        private protected async Task MessageHandler(Telegram.Bot.Types.Message rawMessage)
        {
            try
            {
                if (rawMessage.Text == null) return;
                var message = rawMessage.Text;
                var chatId = rawMessage.Chat.Id;
                Console.WriteLine($"Received a Message in chat {chatId}.");

                await ParseCommand(chatId,message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private protected async Task CallbackHandler(Telegram.Bot.Types.CallbackQuery msg)
        {
            try
            {
                if (msg.Data == null) return;
                Console.WriteLine($"Received a CallbackQuery in UserId {msg.From.Id} and UserName {msg.From.Username} {msg.From.FirstName} {msg.From.LastName} {msg.Data}.");

                var chatId = msg.From.Id;

                await ParseCommand(chatId, msg.Data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private protected async Task HandleErrorAsync(Exception exception)
        {
            throw new NotImplementedException();
        }

        private protected async Task UnknownUpdateHandlerAsync(Update rawUpdate)
        {
            throw new NotImplementedException();
        }

        private protected async Task EditMessageHandler(Message message)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
