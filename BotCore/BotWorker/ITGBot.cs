﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WishGameListBot.BotWorker
{
    public interface ITGBot
    {
        Task SendMessage(long chatId, string text);
    }
}
