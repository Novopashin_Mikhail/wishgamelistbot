﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using WishGameListBot.Commands;

namespace WishGameListBot.BotWorker
{
    public class WebHookBot : BotCore, ITGBot
    {
        public WebHookBot(IOptionsSnapshot<Configuration> config, CommandsCreator creator)
        {
            _config = config.Value;
#if USE_PROXY
            var proxy = _config.Proxy;
            var httpProxy = new WebProxy(proxy.Host, int.Parse(proxy.Port));
            botClient = new TelegramBotClient(_config.BotToken, httpProxy);
#else
            botClient = new TelegramBotClient(_config.BotToken);
#endif
            _creator = creator;
            InitializeWebHook();
        }

        public async Task UpdateHandler(Telegram.Bot.Types.Update rawUpdate)
        {
            var handler = rawUpdate.Type switch
            {
                UpdateType.Message => MessageHandler(rawUpdate.Message),
                UpdateType.EditedMessage => EditMessageHandler(rawUpdate.Message),
                UpdateType.CallbackQuery => CallbackHandler(rawUpdate.CallbackQuery),
                _ => UnknownUpdateHandlerAsync(rawUpdate)
            };

            try
            {
                await handler;
            }
            catch (Exception exception)
            {
                await HandleErrorAsync(exception);
            }

        }

        public async Task SendMessage(long chatId, string text)
        {
            await base.SendMessage(chatId, text);
        }
    }
}