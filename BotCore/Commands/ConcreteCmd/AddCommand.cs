using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Core.Models.RabbitMQ;
using WishGameListBot.BotWorker;
using WishGameListBot.Models;
using WishGameListBot.Rabbit;
using WishGameListBot.ReferenceValidator;
using WishGameListBot.Repositories;

namespace WishGameListBot.Commands
{
    public class AddCommand : CommandBase, ICommand // TODO Добавить валидацию ссылок, при добавлении игры
    {
        private IRepository _repository;
        private IValidator _validator;
        private PublisherTrackedItem _rabbitPublish;
        private IMapper _mapper;

        public override string Name { get; } = "➕ Добавить";
        public override string ConventionName { get; } = "/add";

        public AddCommand()
        { }
        
        public AddCommand(IRepository repository, IValidator validator, PublisherTrackedItem rabbitPublish, IMapper mapper)
        {
            _repository = repository;
            _validator = validator;
            _rabbitPublish = rabbitPublish;
            _mapper = mapper;
        }

        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {
            if (args.Parameters == null) // state addicted command
            {
                botClient.RegisterState(args.ChatId, ConventionName);
                await botClient.SendMessage(args.ChatId, "Введите ссылку на игру в формате https://www.microsoft.com/en-us/p/grand-theft-auto-v-premium-edition/c496clvxmjp8?activetab=pivot:overviewtab");
            }
            else // stateless command
            {
                var url = args.Parameters[0];
                var gameModel = new GameModel
                {
                    Url = url,
                    Guid = Guid.NewGuid()
                };

                var messageValidator = await _validator.IsValidReferenceGameAsync(url);
                if (!messageValidator.IsValid)
                    await botClient.SendMessage(args.ChatId, messageValidator.Message);

                try
                {
                    gameModel.Name = _validator.GetGameName();
                    var gameId = await _repository.AddGameAsync(gameModel);
                    gameModel.Id = gameId;
                    await _repository.AddChatGameAsync(args.ChatId, gameId);

                    var trackedMessage = _mapper.Map<GameModel, TrackedItemMessage>(gameModel);
                    trackedMessage.Action = ItemActions.Add;
                    _rabbitPublish.SendMessage(trackedMessage);
                }
                catch (Exception ex)
                {
                    await botClient.SendMessage(args.ChatId, ex.ToString().Contains("duplicate") ? "Не выполнено. Вы уже подписалиcь на эту игру!" : "Не выполнено");
                }
                await botClient.SendMessage(args.ChatId, messageValidator.Message);
            }
        }
    }
}