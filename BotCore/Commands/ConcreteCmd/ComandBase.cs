﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WishGameListBot.Commands
{
    public abstract class CommandBase
    {
        public virtual string Name { get; }
        public virtual string ConventionName { get; }
    }
}
