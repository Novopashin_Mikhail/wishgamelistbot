﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Core.Models.RabbitMQ;
using WishGameListBot.BotWorker;
using WishGameListBot.Models;
using WishGameListBot.Rabbit;
using WishGameListBot.ReferenceValidator;
using WishGameListBot.Repositories;

namespace WishGameListBot.Commands
{
    public class HelpCommand : CommandBase, ICommand
    {
        public override string Name { get; } = "🔙 Назад";
        public override string ConventionName { get; } = "/help";

        public HelpCommand()
        { }

        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {
            await botClient.SendMessage(args.ChatId, $"Бот способен отслеживать цены на товары в следующих магазинах: {botClient.GetAvailableStores()}", Keyboards.GenerateMainMarkup());
        }
    }
}
