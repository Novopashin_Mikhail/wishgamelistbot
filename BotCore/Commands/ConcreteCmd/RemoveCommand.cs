using System;
using System.Threading.Tasks;
using AutoMapper;
using Core.Models.RabbitMQ;
using WishGameListBot.BotWorker;
using WishGameListBot.Models;
using WishGameListBot.Rabbit;
using WishGameListBot.Repositories;

namespace WishGameListBot.Commands
{
    public class RemoveCommand : CommandBase, ICommand
    {
        private IRepository _repository;
        private PublisherTrackedItem _rabbitPublish;
        private IMapper _mapper;

        public override string Name { get; } = "➖ Удалить";
        public override string ConventionName { get; } = "/rm";

        public RemoveCommand(IRepository repository, PublisherTrackedItem rabbitPublish, IMapper mapper)
        {
            _repository = repository;
            _rabbitPublish = rabbitPublish;
            _mapper = mapper;
        }

        public RemoveCommand()
        { }
        
        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {

            if (args.Parameters == null) // state addicted command
            {
                botClient.RegisterState(args.ChatId, ConventionName);
                await botClient.SendMessage(args.ChatId, "Введите номер удаляемой игры");
            }
            else
            {
                if (!int.TryParse(args.Parameters[0], out int orderedNumberForDelete)) 
                    await botClient.SendMessage(args.ChatId,"Не выполнено");
                var gameModel = await _repository.GetGameByOrderedNumberAsync(orderedNumberForDelete, args.ChatId);
                await _repository.RemoveChatGameAsync(gameModel.Id, args.ChatId);
                var message = _mapper.Map<GameModel, TrackedItemMessage>(gameModel);
                message.Action = ItemActions.Delete;
                _rabbitPublish.SendMessage(message);
                await botClient.SendMessage(args.ChatId, "Выполнено");
            }
        }
    }
}