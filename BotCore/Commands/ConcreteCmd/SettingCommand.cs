using System;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;
using WishGameListBot.BotWorker;
using WishGameListBot.Extensions;
using WishGameListBot.Repositories;

namespace WishGameListBot.Commands
{
    public class SettingCommand : CommandBase, ICommand
    {
        private IRepository _repository;
        
        public SettingCommand(IRepository repository)
        {
            _repository = repository;
        }

        public SettingCommand()
        { }

        public override string Name { get; } = "⏱ Настройки";
        public override string ConventionName { get; } = "/settings";

        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {
            if (args.Parameters == null)
            {
                var chat = await _repository.GetChatAsync(args.ChatId);
                var currentSetting = chat.FrequencyNotifyType.GetDescription();
                await botClient.SendMessage(args.ChatId, $"Ваша текущая настройка: {currentSetting}", Keyboards.GenerateSettingsInlineKeyboard());
            }
            else
            {
                if (int.TryParse(args.Parameters[0], out int numberSetting))
                {
                    await _repository.UpdateSettingsAsync(args.ChatId, (FrequencyNotifyType)numberSetting);
                    await botClient.SendMessage(args.ChatId,"Выполнено");
                }
                else
                    await botClient.SendMessage(args.ChatId, "Не выполнено");
            }
        }
    }
}