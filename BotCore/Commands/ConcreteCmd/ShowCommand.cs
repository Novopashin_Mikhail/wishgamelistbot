using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WishGameListBot.BotWorker;
using WishGameListBot.Repositories;

namespace WishGameListBot.Commands
{
    public class ShowCommand : CommandBase, ICommand
    {
        private IRepository _repository;

        public override string Name { get; } = "Мой список";
        public override string ConventionName { get; } = "/list";

        public ShowCommand(IRepository repository)
        {
            _repository = repository;
        }

        public ShowCommand()
        { }

        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {
            var sb = new StringBuilder();
            var gameList = (await _repository.GetGameListAsync(args.ChatId)).OrderBy(x => x.Url).ToList();
            for (int i = 0; i < gameList.Count; i++)
            {
                sb.AppendLine($"{i + 1}. {gameList[i].Name} {gameList[i].Url}");
            }
            var list = sb.ToString();
            string result = string.IsNullOrEmpty(list) ? "\tПусто." : list;
            await botClient.SendMessage(args.ChatId, $"Ваш список игр:\n{result}", Keyboards.GenerateGameListMarkup());
        }
    }
}