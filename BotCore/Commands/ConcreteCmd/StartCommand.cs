﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WishGameListBot.BotWorker;
using WishGameListBot.Extensions;
using WishGameListBot.Repositories;
using Telegram.Bot.Types.ReplyMarkups;

namespace WishGameListBot.Commands
{
    public class StartCommand : CommandBase, ICommand
    {
        private IRepository _repository;

        public override string Name { get; }
        public override string ConventionName { get; } = "/start";

        public StartCommand()
        { }

        public StartCommand(IRepository repository)
        {
            _repository = repository;
        }

        public async Task ProcessAsync(CommandArgs args, BotCore botClient)
        {
            botClient.RegisterUser(args.ChatId);
            await _repository.AddChatAsync(args.ChatId);
            
            await botClient.SendMessage(args.ChatId, $"Этот бот умеет отслеживать скидки на игры, которые Вы собираетесь приобрести, " +
                                      $"на данный момент, умеет отслеживать цены для {botClient.GetAvailableStores()}", Keyboards.GenerateMainMarkup());
        }
    }
}
