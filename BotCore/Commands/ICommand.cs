using System.Threading.Tasks;
using WishGameListBot.BotWorker;

namespace WishGameListBot.Commands
{
    public interface ICommand
    {
        Task ProcessAsync(CommandArgs args, BotCore botClient);
    }
}