namespace WishGameListBot.Commands
{
    public class CommandArgs
    {
        public string Input { get; set; }
        public long ChatId { get; set; }
        public string[] Parameters { get; set; }
    }
}