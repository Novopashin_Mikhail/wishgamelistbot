using System;
using System.Collections.Generic;
using System.Linq;

namespace WishGameListBot.Commands
{
    public class CommandList
    {
        public Type CmdType;
        public string Cmd;
        public string ConventionCmd;

        public bool IsAvailableCmd(string cmd)
        {
            return (cmd == Cmd) || (ConventionCmd == cmd);
        }
    }
    public class CommandsCreator
    {
        private IServiceProvider _serviceProvider;

        private static List<CommandList> _cmdList;

        static CommandsCreator()
        {
            _cmdList = new List<CommandList>();

            var commands = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
                     .Where(x => typeof(CommandBase).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).ToList();
            foreach (var cmd in commands)
            {

                var instance = Activator.CreateInstance(cmd) as CommandBase;
                _cmdList.Add(new CommandList 
                    {   CmdType = cmd, 
                        Cmd = instance.Name, 
                        ConventionCmd = instance.ConventionName }
                );
            }
        }

        public CommandsCreator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        public ICommand Create(string command)
        {
            foreach (var item in _cmdList)
                if (item.IsAvailableCmd(command))
                    return (ICommand)_serviceProvider.GetService(item.CmdType);

            //if (command.Equals(StateChat.Add.ToString()))
            //    return (ICommand)_serviceProvider.GetService(typeof(AddCommand));
            //if (command.Equals(StateChat.Remove.ToString()))
            //    return (ICommand)_serviceProvider.GetService(typeof(RemoveCommand));
            //if (command.Equals(StateChat.ShowList.ToString()))
            //    return (ICommand)_serviceProvider.GetService(typeof(ShowCommand));
            //if (command.Equals(StateChat.Setting.ToString()))
            //    return (ICommand)_serviceProvider.GetService(typeof(SettingCommand));

            return null;
        }
    }
}