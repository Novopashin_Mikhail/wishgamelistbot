using System.ComponentModel;

namespace WishGameListBot.Commands
{
    public enum FrequencyNotifyType
    {
        [Description("1 раз в час")]
        OneTimeHour = 1,
        [Description("1 раз в день")]
        OneTimeDay = 2,
        [Description("1 раз в неделю")]
        OneTimeWeek = 3
    }
}