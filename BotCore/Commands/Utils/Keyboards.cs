﻿using System;
using System.Collections.Generic;
using System.Text;
using Telegram.Bot.Types.ReplyMarkups;

namespace WishGameListBot.Commands
{
    internal class Keyboards
    {
        internal static ReplyKeyboardMarkup GenerateMainMarkup()
        {
            var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                    new KeyboardButton[][]
                    {
                        new KeyboardButton[] { "Мой список", "⏱ Настройки" },
                    },
                    resizeKeyboard: true
                );
            return replyKeyboardMarkup;
        }

        internal static ReplyKeyboardMarkup GenerateGameListMarkup()
        {
            var replyKeyboardMarkup = new ReplyKeyboardMarkup(
                    new KeyboardButton[][]
                    {
                        new KeyboardButton[] { "➕ Добавить", "➖ Удалить", "🔙 Назад" },
                    },
                    resizeKeyboard: true
                );
            return replyKeyboardMarkup;
        }

        internal static InlineKeyboardMarkup GenerateSettingsInlineKeyboard()
        {
            var inlineKeyboard = new InlineKeyboardMarkup(new[]
                {
                    new []
                    {
                        InlineKeyboardButton.WithCallbackData("Раз в час", "/settings 1"),
                        InlineKeyboardButton.WithCallbackData("Раз в день", "/settings 2"),
                        InlineKeyboardButton.WithCallbackData("Раз в неделю", "/settings 3"),
                    }
                });
            return inlineKeyboard;
        }
    }
}
