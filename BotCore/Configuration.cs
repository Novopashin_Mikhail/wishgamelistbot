﻿namespace WishGameListBot
{
    public class Configuration
    {
        public string BotToken { get; set; }
        public string[] AvailableStore { get; set; }
        public string ConnectionString { get; set; }
        public RabbitConfiguration RabbitConfiguration { get; set; }
        public string ExternalURL { get; set; }
        public Proxy Proxy { get; set; }
    }

    public class RabbitConfiguration
    {
        public string HostName { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DiscountsQueueName { get; set; }
        public string DiscountsExchangeName { get; set; }
        public string TrackedItemsQueueName { get; set; }
    }

    public class Proxy
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}