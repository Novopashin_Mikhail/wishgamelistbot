namespace WishGameListBot
{
    public class CustomKeyboardItem
    {
        public CustomKeyboardItem(string text, string command)
        {
            Text = text;
            Command = command;
        }
        public string Text { get; set; }
        public string Command { get; set; }
    }
}