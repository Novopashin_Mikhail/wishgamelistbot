using System.Collections.Generic;
using WishGameListBot.Commands;

namespace WishGameListBot.Entities
{
    public class ChatEntity
    {
        public int Id { get; set; }
        public long ChatId { get; set; }
        public FrequencyNotifyType? FrequencyNotifyType { get; set; }
        public List<ChatGameEntity> ChatGameEntities { get; set; }
    }
}