using System;

namespace WishGameListBot.Entities
{
    public class ChatGameEntity
    {
        public int Id { get; set; }
        public int ChatId { get; set; }
        public int GameId { get; set; }
        public DateTime? LastNotify { get; set; }
        
        public ChatEntity ChatEntity { get; set; }
        public GameEntity GameEntity { get; set; }
    }
}