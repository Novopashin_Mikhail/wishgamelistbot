using System;
using System.Collections.Generic;

namespace WishGameListBot.Entities
{
    public class GameEntity
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public List<ChatGameEntity> ChatGameEntities { get; set; }
    }
}