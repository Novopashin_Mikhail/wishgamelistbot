﻿using System;
using System.Linq;
using System.Reflection;
using Core.Models.RabbitMQ;

namespace WishGameListBot.Extensions
{
    public static class EnumExtensionMethods
    {
        public static string GetDescription(this Enum genericEnum)
        {
            var genericEnumType = genericEnum.GetType();
            var memberInfo = genericEnumType.GetMember(genericEnum.ToString());
            if ((memberInfo.Length <= 0)) return genericEnum.ToString();
            var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
            if (_Attribs != null && _Attribs.Length > 0)
            {
                return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
            }
            return genericEnum.ToString();
        }

        public static string GetMessage(this DiscountMessage discMessage)
        {
            return $"Скида {Math.Round(discMessage.DiscountPercent,0)}% на {discMessage.ItemName} которая в списке Ваших желаний, " +
                   $"ее стоимость составит {discMessage.DiscountPrice} {discMessage.Currency} вместо {discMessage.FullPrice} {discMessage.Currency}. Торопитесь!";
        }
    }
}