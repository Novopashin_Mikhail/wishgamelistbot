using AutoMapper;
using Core.Models.RabbitMQ;
using WishGameListBot.Entities;
using WishGameListBot.Models;

namespace WishGameListBot.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<GameModel, GameEntity>()
                .ReverseMap();
            
            CreateMap<ChatModel, ChatEntity>()
                .ReverseMap();

            CreateMap<GameModel, TrackedItemMessage>()
                .ForMember(x => x.ItemGuid, y => y.MapFrom(z => z.Guid))
                .ForMember(x => x.ItemLink, y => y.MapFrom(z => z.Url))
                .ForMember(x => x.ItemName, y => y.MapFrom(z => z.Name))
                .ReverseMap();
        }
    }
}