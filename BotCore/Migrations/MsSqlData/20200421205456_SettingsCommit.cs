﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WishGameListBot.Migrations.MsSqlData
{
    public partial class SettingsCommit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastNotify",
                table: "ChatGameEntity",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FrequencyNotifyType",
                table: "ChatEntity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ChatEntity_ChatId",
                table: "ChatEntity",
                column: "ChatId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ChatEntity_ChatId",
                table: "ChatEntity");

            migrationBuilder.DropColumn(
                name: "LastNotify",
                table: "ChatGameEntity");

            migrationBuilder.DropColumn(
                name: "FrequencyNotifyType",
                table: "ChatEntity");
        }
    }
}
