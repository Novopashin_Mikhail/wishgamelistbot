﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WishGameListBot.Migrations.MsSqlData
{
    public partial class UniqueChatIdGameId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ChatGameEntity_ChatId_GameId",
                table: "ChatGameEntity",
                columns: new[] { "ChatId", "GameId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ChatGameEntity_ChatId_GameId",
                table: "ChatGameEntity");
        }
    }
}
