﻿using WishGameListBot.Commands;

namespace WishGameListBot.Models
{
    public class ChatModel
    {
        public int Id { get; set; }
        public long ChatId { get; set; }
        public FrequencyNotifyType? FrequencyNotifyType { get; set; }
    }
}