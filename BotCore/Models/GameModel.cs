using System;

namespace WishGameListBot.Models
{
    public class GameModel
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}