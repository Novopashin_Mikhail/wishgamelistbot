﻿using System;
using WishGameListBot.Commands;

namespace WishGameListBot.Models
{
    public class NotifyModel
    {
        public long ChatId { get; set; }
        public FrequencyNotifyType FrequencyNotifyType { get; set; }
        public DateTime? LastUpdateForNotifyingGame { get; set; }
    }
}