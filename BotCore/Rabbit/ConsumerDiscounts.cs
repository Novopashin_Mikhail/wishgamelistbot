﻿using System;
using System.Text;
using AutoMapper;
using Core.Models.RabbitMQ;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using WishGameListBot.Models;
using WishGameListBot.Services;

namespace WishGameListBot.Rabbit
{
    public class ConsumerDiscounts : IDisposable
    {
        private RabbitConfiguration _configuration;
        private IConnection _connection;
        private IModel _channel;
        private IServiceProvider _provider;
        
        public ConsumerDiscounts(IOptionsSnapshot<Configuration> configuration, IServiceProvider provider)
        {
            _configuration = configuration.Value.RabbitConfiguration;
            _provider = provider;
        }
        
        private int _unacked = 0;
        private int _acked = 0;
        public void Run()
        {
            var factory = new ConnectionFactory()
            {
                HostName = _configuration.HostName,
                VirtualHost = _configuration.VirtualHost,
                UserName = _configuration.UserName,
                Password = _configuration.Password
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_configuration.DiscountsExchangeName, ExchangeType.Topic);
            _channel.QueueBind(_configuration.DiscountsQueueName, _configuration.DiscountsExchangeName, "#");

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (model, ea) =>
            {
                using var scope = _provider.CreateScope();
                var notifierService = scope.ServiceProvider.GetService<INotifierService>();
                    
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var discountMessage = JsonConvert.DeserializeObject<DiscountMessage>(message);
                await notifierService.NotifyAsync(discountMessage);
                _channel.BasicAck(ea.DeliveryTag, multiple: false);
            };
            _channel.BasicConsume(queue: _configuration.DiscountsQueueName,
                autoAck: false,
                consumer: consumer);
        }

        public void Dispose()
        {
            _connection?.Dispose();
            _channel?.Dispose();
        }
    }
}