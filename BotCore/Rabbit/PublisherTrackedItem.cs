﻿using System;
using System.Text;
using System.Threading;
using Core.Models.RabbitMQ;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace WishGameListBot.Rabbit
{
    public class PublisherTrackedItem
    {
        private RabbitConfiguration _configuration;
        public PublisherTrackedItem(IOptionsSnapshot<Configuration> configuration)
        {
            _configuration = configuration.Value.RabbitConfiguration;
        }
        
        public void SendMessage(TrackedItemMessage trackedItem)
        {
            var factory = new ConnectionFactory()
            {
                HostName = _configuration.HostName,
                VirtualHost = _configuration.VirtualHost,
                UserName = _configuration.UserName,
                Password = _configuration.Password
            };
            using(var connection = factory.CreateConnection())
            using(var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(exchange: _configuration.TrackedItemsQueueName,
                    type: "topic");

                var routingKey = trackedItem.ItemLink;
                var message = JsonConvert.SerializeObject(trackedItem);
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: _configuration.TrackedItemsQueueName,
                    routingKey: routingKey,
                    basicProperties: null,
                    body: body);
                Console.WriteLine(" [x] Sent {0}", message);
            }
        }
    }
}