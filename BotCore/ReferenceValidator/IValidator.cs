﻿using System.Threading.Tasks;

namespace WishGameListBot.ReferenceValidator
{
    public interface IValidator
    {
        Task<ValidMessage> IsValidReferenceGameAsync(string url);
        string GetGameName();
    }
}