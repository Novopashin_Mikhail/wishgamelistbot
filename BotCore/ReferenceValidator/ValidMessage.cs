﻿namespace WishGameListBot.ReferenceValidator
{
    public class ValidMessage
    {
        public ValidMessage(){}

        public ValidMessage(bool isValid = false, string message = null)
        {
            IsValid = isValid;
            Message = message;
        }
        
        public bool IsValid { get; set; }
        public string Message { get; set; }
    }
}