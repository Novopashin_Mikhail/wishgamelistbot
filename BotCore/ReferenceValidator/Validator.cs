﻿using System;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using Microsoft.Extensions.Options;
using ProductInfo = WishGameListBot.Models.ProductInfo;

namespace WishGameListBot.ReferenceValidator
{
    public class Validator : IValidator
    {
        private string _gameName;
        private Configuration _config;

        public Validator(IOptionsSnapshot<Configuration> config)
        {
            _config = config.Value;
        }
        public async Task<ValidMessage> IsValidReferenceGameAsync(string url)
        {
            _gameName = string.Empty;

            if (!CheckURLValid(url)) return new ValidMessage(false, "Не верный формат. Это скорее всего не ссылка!");
            if (!CheckContainSupportedDomain(url)) return new ValidMessage(false, "Этот магазин мы еще не поддерживаем.");
            
            var config = AngleSharp.Configuration.Default.WithDefaultLoader();           
            var context = BrowsingContext.New(config);
            var document = await context.OpenAsync(url);
            
            foreach(var script in document.Scripts)
            {
                var match = Regex.Match(script.Text, "(?<=ProductInfo:)[^}]+}");
                if (string.IsNullOrEmpty(match?.Value)) continue;
                var productInfo = JsonSerializer.Deserialize<ProductInfo>(match.Value);
                _gameName = productInfo.title;
            }

            var isValid = !string.IsNullOrEmpty(_gameName);
            var res = new ValidMessage(isValid);
            res.Message = res.IsValid ? "Выполнено" : "Произошла ошибка повторите позднее";
            return res;
        }
        
        public bool CheckURLValid(string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult)
                   && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public bool CheckContainSupportedDomain(string url)
        {
            return _config.AvailableStore.Any(url.Contains);
        }

        public string GetGameName()
        {
            return _gameName;
        }
    }
}