
using System.Collections.Generic;
using System.Threading.Tasks;
using WishGameListBot.Commands;
using WishGameListBot.Models;

namespace WishGameListBot.Repositories
{
    public interface IRepository
    {
        Task AddChatAsync(long chatId);
        Task<int> AddGameAsync(GameModel game);
        Task RemoveGameAsync(string url);
        Task<IList<GameModel>> GetGameListAsync(long chatId); 
        Task AddChatGameAsync(long chatId, int gameId);
        Task<GameModel> GetGameByOrderedNumberAsync(int orderedNumber, long chatId);
        Task RemoveChatGameAsync(int gameId, long chatId);
        Task UpdateSettingsAsync(long chatId, FrequencyNotifyType type);
        Task<ChatModel> GetChatAsync(long chatId);
        Task<List<long>> GetChatsByGameWhichNeedNotifyAsync(string url);
        Task UpdateLastNotifyAsync(List<long> chatIds, string url);
    }
}