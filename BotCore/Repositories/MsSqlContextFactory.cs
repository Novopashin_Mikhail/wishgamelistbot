using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace WishGameListBot.Repositories
{
    public class MsSqlContextFactory : IDesignTimeDbContextFactory<MsSqlDataContext>
    {
        private string connectionString = "Server=tcp:otusbot.database.windows.net,1433;Initial Catalog=botServiceDb;Persist Security Info=False;User ID=nanoadmen;Password=Abc123456;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        public MsSqlDataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MsSqlDataContext>();
            optionsBuilder.UseSqlServer(connectionString);
            return new MsSqlDataContext(optionsBuilder.Options);
        }
    }
}