﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WishGameListBot.Entities;

namespace WishGameListBot.Repositories
{
    public class MsSqlDataContext : DbContext
    {
        private Configuration _config;
        
        public MsSqlDataContext(DbContextOptions<MsSqlDataContext> options) :
            base(options)
        {
        }

        public DbSet<ChatEntity> ChatEntity { get; set; }
        public DbSet<GameEntity> GameEntity { get; set; }
        public DbSet<ChatGameEntity> ChatGameEntity { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<GameEntity>()
                .HasIndex(g => g.Url)
                .IsUnique();
            
            builder.Entity<ChatEntity>()
                .HasIndex(g => g.ChatId)
                .IsUnique();

            builder.Entity<ChatGameEntity>()
                .HasIndex(g => new {g.ChatId, g.GameId})
                .IsUnique();
            
            builder.Entity<ChatGameEntity>()
                .HasOne(x => x.GameEntity)
                .WithMany(y => y.ChatGameEntities)
                .HasForeignKey(z => z.GameId);  
            builder.Entity<ChatGameEntity>()
                .HasOne(x => x.ChatEntity)
                .WithMany(y => y.ChatGameEntities)
                .HasForeignKey(z => z.ChatId);

        }
    }
}