using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WishGameListBot.Commands;
using WishGameListBot.Entities;
using WishGameListBot.Models;

namespace WishGameListBot.Repositories
{
    public class Repository : IRepository
    {
        private MsSqlDataContext _context; 
        private IMapper _mapper;
       
        public Repository(MsSqlDataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
       
        public async Task AddChatAsync(long chatId)
        {
            try
            {
                var chat = await _context.ChatEntity.FirstOrDefaultAsync(x => x.ChatId == chatId);
                if (chat != null) return;
                await _context.ChatEntity.AddAsync(new ChatEntity{ChatId = chatId, FrequencyNotifyType = FrequencyNotifyType.OneTimeDay});
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        
        public async Task<int> AddGameAsync(GameModel game)
        {
            try
            {
                var gameEntity = _mapper.Map<GameEntity>(game);
                var gameEntityInDb = _context.GameEntity.FirstOrDefault(x => x.Url == game.Url);
                if (gameEntityInDb != null) return gameEntityInDb.Id;
                await _context.GameEntity.AddAsync(gameEntity);
                await _context.SaveChangesAsync();
                return gameEntity.Id;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        
        public async Task RemoveGameAsync(string url)
        {
            var gameEntity = _context.GameEntity.FirstOrDefault(x => x.Url.Equals(url));
            if (gameEntity == null) return;
            _context.GameEntity.Remove(gameEntity);
            await _context.SaveChangesAsync();
        }
        
        public async Task<IList<GameModel>> GetGameListAsync(long chatId)
        {
            var chatEntity = await CheckChatEntityGetIdAsync(chatId);
            var gameIds = await _context.ChatGameEntity.Where(x => x.ChatId == chatEntity.Id).Select(x => x.GameId).ToListAsync();
            var gameEntityList = await _context.GameEntity.Where(x => gameIds.Contains(x.Id)).ToListAsync();
            return gameEntityList.Select(x => _mapper.Map<GameModel>(x)).ToList();
        }
        
        public async Task AddChatGameAsync(long chatId, int gameId)
        {
            var chatEntity = await CheckChatEntityGetIdAsync(chatId);
            var chatGame = new ChatGameEntity
            {
                ChatId = chatEntity.Id,
                GameId = gameId
            };
            await _context.ChatGameEntity.AddAsync(chatGame);
            await _context.SaveChangesAsync();
        }

        private async Task<ChatEntity> CheckChatEntityGetIdAsync(long chatId)
        {
            var chatEntity = await _context.ChatEntity.FirstOrDefaultAsync(x => x.ChatId == chatId);
            if (chatEntity == null)
            {
                chatEntity = new ChatEntity {ChatId = chatId};
                await _context.ChatEntity.AddAsync(chatEntity);
            }

            return chatEntity;
        }
        

        public async Task<GameModel> GetGameByOrderedNumberAsync(int orderedNumber, long chatId)
        {
            var gameModel = (await GetGameListAsync(chatId)).OrderBy(x => x.Url).ToList()[orderedNumber-1];
            return gameModel;
        }
        
        public async Task RemoveChatGameAsync(int gameId, long chatId)
        {
            var chatEntity = await CheckChatEntityGetIdAsync(chatId);
            var chatGameEntity = _context.ChatGameEntity.Where(x => x.ChatId == chatEntity.Id && x.GameId == gameId);
            if (!chatGameEntity.Any()) return;
            _context.ChatGameEntity.RemoveRange(chatGameEntity);
            await _context.SaveChangesAsync();
        }
        
        public async Task UpdateSettingsAsync(long chatId, FrequencyNotifyType type)
        {
            var chat = await _context.ChatEntity.FirstOrDefaultAsync(x => x.ChatId == chatId);
            chat.FrequencyNotifyType = type;
            await _context.SaveChangesAsync();
        }

        public async Task<ChatModel> GetChatAsync(long chatId)
        {
            var chat = await _context.ChatEntity.FirstOrDefaultAsync(x => x.ChatId == chatId);
            return _mapper.Map<ChatEntity, ChatModel>(chat);
        }

        public async Task<List<long>> GetChatsByGameWhichNeedNotifyAsync(string url)
        {
            var gameEntity = await _context.GameEntity
                    .Include(x => x.ChatGameEntities)
                    .ThenInclude(x => x.ChatEntity)
                    .FirstOrDefaultAsync(x => x.Url.Equals(url));

                var chats = gameEntity.ChatGameEntities.Select(x => x.ChatEntity);
                var getLastNotifyNull = gameEntity.ChatGameEntities
                    .Where(x => x.LastNotify == null);
                var getRightChatGameEntity = gameEntity.ChatGameEntities
                    .Where(x => x.LastNotify != null && (x.LastNotify - DateTime.Now).Value.Hours
                        >= GetHourByFrequencyNotifyType(x.ChatEntity.FrequencyNotifyType));
                var resultChatDbId = getLastNotifyNull.Union(getRightChatGameEntity).Select(x => x.ChatId);
                var result = chats.Where(x => resultChatDbId.Contains(x.Id)).Select(x => x.ChatId).ToList();
                return result;

 
        }

        public async Task UpdateLastNotifyAsync(List<long> chatIds, string url)
        {
            var chatEntities = await _context.ChatEntity.Where(x => chatIds.Contains(x.ChatId)).ToListAsync();
            var game = await _context.GameEntity.FirstOrDefaultAsync(x => x.Url == url);
            var entities = _context.ChatGameEntity.Where(x => chatEntities.Select(y => (long)y.Id).Contains(x.ChatId) && x.GameId == game.Id);
            await entities.ForEachAsync(x => x.LastNotify = DateTime.Now);
            await _context.SaveChangesAsync();
        }

        private int GetHourByFrequencyNotifyType(FrequencyNotifyType? notifyType)
        {
            return notifyType switch
            {
                FrequencyNotifyType.OneTimeHour => 1,
                FrequencyNotifyType.OneTimeDay => 24,
                FrequencyNotifyType.OneTimeWeek => 168,
                _ => 0
            };
        }
    }
} 