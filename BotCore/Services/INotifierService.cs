﻿using System.Threading.Tasks;
using Core.Models.RabbitMQ;

namespace WishGameListBot.Services
{
    public interface INotifierService
    {
        Task NotifyAsync(DiscountMessage messageModel);
    }
}