﻿using System.Threading.Tasks;
using Core.Models.RabbitMQ;
using Microsoft.Extensions.Options;
using Telegram.Bot;
using WishGameListBot.BotWorker;
using WishGameListBot.Extensions;
using WishGameListBot.Repositories;

namespace WishGameListBot.Services
{
    public class NotifierService : INotifierService
    {
        private Configuration _config;
        private ITGBot _bot;
        private IRepository _repository;

        public NotifierService(IOptionsSnapshot<Configuration> config, IRepository repository, ITGBot bot)
        {
            _config = config.Value;
            _bot = bot;
            _repository = repository;
        }
        
        public async Task NotifyAsync(DiscountMessage messageModel)
        {
            var chatIds = await _repository.GetChatsByGameWhichNeedNotifyAsync(messageModel.ItemLink);
            if (chatIds == null) return;
            foreach (var chatId in chatIds)
            {
                await SendMessageAsync(chatId, messageModel.GetMessage());
            }

            await _repository.UpdateLastNotifyAsync(chatIds, messageModel.ItemLink);
        }
        
        private async Task SendMessageAsync(long chatId, string text)
        {
            await _bot.SendMessage(
                chatId: chatId,
                text: text
            );
        }
    }
}