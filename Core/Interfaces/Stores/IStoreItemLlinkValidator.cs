﻿using Core.Models.Stores;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Interfaces.Stores
{
    public interface IStoreItemLlinkValidator
    {
        Task<ItemLinkValidationResult> ValidateStoreItemLinkAsync(string itemLink, CancellationToken token);
    }
}
