﻿using Core.Models.Stores;
using System.Threading.Tasks;

namespace Core.Interfaces.Stores
{
    public interface IStoreItemPriceService
    {
        Task<ItemPrice> GetCurentPriceAsync(string itemLink);
    }
}
