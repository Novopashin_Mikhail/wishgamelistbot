﻿using System.Threading.Tasks;

namespace Core
{
    public interface IStoreParser
    {
        Task<PriceHistory> GetPriceHistory(string url);
    }
}
