﻿namespace Core.Models
{
    public enum Currency
    {
        Ruble = 0,
        Dollar = 1,
        Euro = 2
    }
}
