﻿using System;

namespace Core.Models.RabbitMQ
{
    public class DiscountMessage
    {
        public Guid ItemGuid { get; set; }
        public string ItemName { get; set; }
        public string ItemLink { get; set; }
        public decimal FullPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public decimal DiscountPercent => FullPrice != 0 ? (100 - DiscountPrice * 100 / FullPrice) : 0;
        public Currency Currency { get; set; }
    }
}
