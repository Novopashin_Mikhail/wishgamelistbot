﻿using System;

namespace Core.Models.RabbitMQ
{
    public class TrackedItemMessage
    {
        public Guid ItemGuid { get; set; }
        public string ItemName { get; set; }
        public string ItemLink { get; set; }
        public ItemActions Action { get; set; }
    }
}
