﻿namespace Core.Models.Stores
{
    public class ItemLinkValidationResult
    {
        public string ItemName { get; set; }

        public string ItemLink { get; set; }

        public bool IsValid { get; set; }
    }
}
