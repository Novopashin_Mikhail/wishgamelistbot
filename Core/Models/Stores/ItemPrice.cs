﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.Stores
{
    public class ItemPrice
    {
        public decimal FullPrice { get; set; }
        public decimal DiscountPrice { get; set; }
    }
}
