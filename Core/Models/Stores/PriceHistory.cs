using System;

namespace Core
{
    public class PriceHistory
    {
        
        public int Id { get; set; }
        
        public DateTime DateTime { get; set; }
        
        public string NameGame { get; set; }
        
        public decimal FirstPrice { get; set; }
        public decimal LastPrice { get; set; }
        public int Currency { get; set; }

    }
}