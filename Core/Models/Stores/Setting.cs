
namespace Core
{
    public class Setting
    {   
        public string ConnectionString { get; set; }
        public string ConnectionHangfire { get; set; }
        public string HostName { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }       
        public string ExchangePrice { get; set; }
        public string ExchangeTrack { get; set; }
        public string QueueName { get; set; }
        public string BindingKey { get; set; }
        public string CronTrackService { get; set; }
        public string CronMonitorStoreService { get; set; }
    }
}