using System;

namespace Core
{
    public class TrackItem
    {
        
        public int Id { get; set; }
        
        public Guid IdUser { get; set; }
        
        public string NameGame { get; set; }
        
        public string Url { get; set; }
        public decimal FirstPrice { get; set; }
        public decimal LastPrice { get; set; }

    }
}