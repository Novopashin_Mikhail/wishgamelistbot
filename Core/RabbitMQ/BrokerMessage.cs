﻿using RabbitMQ.Client;

namespace Core
{
    public class BrokerMessage
    {
        protected readonly Setting _setting;       
        public BrokerMessage(Setting setting)
        {
            _setting = setting;            
        }       
        protected IModel GetModel(IConnection connection, string exchangeName)
        {           
            var model = connection.CreateModel();
            model.ExchangeDeclare(exchangeName, ExchangeType.Topic);
                       
            return model;
        }
        protected IConnection GetConnection()
        {
            var factory = new ConnectionFactory()
            {
                HostName = _setting.HostName,
                VirtualHost = _setting.VirtualHost,
                UserName = _setting.UserName,
                Password = _setting.Password
            };

            return factory.CreateConnection();
        }        
    }
}
