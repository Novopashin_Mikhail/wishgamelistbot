﻿using System;
using System.Threading;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Core
{
    public class Consumer : BrokerMessage
    {
        private readonly ILogger<string> _logger;
        private readonly string _queueName;
        private readonly string _bindingKey;
        public Consumer(IOptions<Setting> options, ILogger<string> logger) : base(options.Value)
        {
            _logger = logger;
            _queueName = options.Value.QueueName;
            _bindingKey = options.Value.BindingKey;
        }
        public void Receive(string exchangeName, Action<BasicDeliverEventArgs> action)
        {           
            try
            {
                using var connection = GetConnection();
                using var model = GetModel(connection, exchangeName);
                model.QueueBind(_queueName, exchangeName, _bindingKey);

                var consumer = new EventingBasicConsumer(model);
                consumer.Received += (sender, ea) =>
                {                    
                    action(ea);                    
                };

                model.BasicConsume(_queueName, true, consumer);

                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }            
        }
    }
}
