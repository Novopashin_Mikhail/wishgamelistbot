﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Core
{
    public class Producer<T> : BrokerMessage
    {
        private readonly ILogger<string> _logger;
        public Producer(IOptions<Setting> options, ILogger<string> logger) : base(options.Value)
        {
            _logger = logger;
        }
        public async Task Send(string exchangeName, T entity, string routingKey = "#")
        {
            try
            {
                using var connection = GetConnection();
                using var model = GetModel(connection, exchangeName);
                await Task.Run(() => model.BasicPublish(exchangeName, routingKey, null, Encoding.UTF8.GetBytes(JsonSerializer.Serialize(entity).ToString())));
               
                _logger.LogInformation("Message send success!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}
