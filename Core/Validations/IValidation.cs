﻿
namespace Core
{
    public interface IValidation<T>
    {
        bool IsValidModel(T model);
    }
}
