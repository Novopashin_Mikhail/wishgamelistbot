﻿using Microsoft.Extensions.Logging;
using FluentValidation;

namespace Core
{
    public class Validation<T> : IValidation<T>
    {        
        private readonly AbstractValidator<T> _validator;
        private readonly ILogger<string> _logger;
        public Validation(AbstractValidator<T> validator, ILogger<string> logger)
        {            
            _validator = validator;
            _logger = logger;
        }
        public bool IsValidModel(T model)
        {
            if (model == null)
            {
                return false;
            }
                        
            var resultValidation = _validator.Validate(model);
            if (!resultValidation.IsValid)
            {
                _logger.LogError(resultValidation.ToString(";"));                
                return false;
            }

            return true;            
        }
    }
}
