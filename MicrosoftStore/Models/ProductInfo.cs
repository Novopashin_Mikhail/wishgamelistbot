
using System.Text.Json.Serialization;

namespace StoreService
{
    public class ProductInfo
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("priCat")]
        public string PriCat { get; set; }
        [JsonPropertyName("type")]
        public string Type { get; set; }
        [JsonPropertyName("family")]
        public string Family { get; set; }
        [JsonPropertyName("conRtg")]
        public string ConRtg { get; set; }
        [JsonPropertyName("lstPrice")]
        public decimal LastPrice { get; set; }
        [JsonPropertyName("rtPrice")]
        public decimal FirstPrice { get; set; }
        [JsonPropertyName("cur")]
        public string Currency { get; set; }
        [JsonPropertyName("sku")]
        public string Sku { get; set; }
        [JsonPropertyName("skuType")]
        public string SkuType { get; set; }
        [JsonPropertyName("isChild")]
        public string IsChild { get; set; }

    }
}