﻿using System;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AngleSharp;
using Core;

namespace StoreService
{
    public class MicrosoftParser : IStoreParser
    {       
        public async Task<PriceHistory> GetPriceHistory(string url)
        {
            var config = Configuration.Default.WithDefaultLoader();            
            var context = BrowsingContext.New(config);
            var document = await context.OpenAsync(url);

            foreach(var script in document.Scripts)
            {                
                var match = Regex.Match(script.Text, "(?<=ProductInfo:)[^}]+}");
                if (match!=null)
                {
                    var productInfo = JsonSerializer.Deserialize<ProductInfo>(match.Value);
                    return new PriceHistory() 
                    { 
                        DateTime = DateTime.Now, 
                        NameGame = productInfo.Title, 
                        FirstPrice = productInfo.FirstPrice, 
                        LastPrice = productInfo.LastPrice, 
                        Currency = GetCurrency(productInfo.Currency) 
                    };
                }
            }
            
            return null;
        }

        private int GetCurrency(string strCurrency)
        {
            return strCurrency switch
            {
                "USD" => 1,                
                _ => 0
            };
        }
    }
}
 