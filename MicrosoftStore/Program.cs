﻿using Microsoft.Extensions.Hosting;

namespace MicrosoftStore
{
    class Program
    {
        static void Main()
        {
           Startup.CreateHostBuilder().Build().Run();
        }
    }
}
