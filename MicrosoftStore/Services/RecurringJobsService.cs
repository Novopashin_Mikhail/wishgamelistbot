﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.Server;
using Core;
using StoreService;

namespace MicrosoftStore
{
    internal class RecurringJobsService : BackgroundService
    {
        private readonly Setting _setting;
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ILogger<RecurringJobScheduler> _logger;        
        private readonly TrackService _trackService;
        private readonly MonitorStoreService _monitorStoreService;
        public RecurringJobsService(IOptions<Setting> options, IRecurringJobManager recurringJobs, ILogger<RecurringJobScheduler> logger,
                                    TrackService trackService, MonitorStoreService monitorStoreService)
        {
            _setting = options.Value;
            _recurringJobs = recurringJobs;
            _logger = logger;
            _trackService = trackService;           
            _monitorStoreService = monitorStoreService;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _recurringJobs.AddOrUpdate("TrackService", () => _trackService.Execute(_setting.ExchangeTrack), _setting.CronTrackService);                
                _recurringJobs.AddOrUpdate("MonitorStoreService", () => _monitorStoreService.Execute(), _setting.CronMonitorStoreService);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Task.CompletedTask;
        }
    }
}
