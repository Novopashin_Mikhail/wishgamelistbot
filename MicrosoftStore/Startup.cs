﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Hangfire;
using Hangfire.SqlServer;
using FluentValidation;
using Core;
using Core.Models.RabbitMQ;
using StoreService;

namespace MicrosoftStore
{
    public static class Startup
    {
        public static IHostBuilder CreateHostBuilder()
        {
            return new HostBuilder()
                   .ConfigureLogging(x => x.AddConsole().SetMinimumLevel(LogLevel.Information))
                   .ConfigureAppConfiguration((context, config) =>
                   {
                       config.AddJsonFile("appsettings.json", optional: false);
                   })
                   .ConfigureServices((context, services) =>
                   {
                       services.Configure<Setting>(context.Configuration);
                       services.AddDbContext<DataContext>(options =>
                       {
                           options.UseSqlServer(context.Configuration["ConnectionString"]);
                       });
                       services.TryAddSingleton(new SqlServerStorageOptions
                       {
                           CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                           QueuePollInterval = TimeSpan.FromTicks(1),
                           UseRecommendedIsolationLevel = true,
                           SlidingInvisibilityTimeout = TimeSpan.FromMinutes(1)
                       });
                       services.AddHangfire((provider, configuration) => configuration
                           .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                           .UseSimpleAssemblyNameTypeSerializer()
                           .UseSqlServerStorage(context.Configuration["ConnectionHangfire"], provider.GetRequiredService<SqlServerStorageOptions>())
                       );
                       services.AddHangfireServer(options =>
                       {
                           options.StopTimeout = TimeSpan.FromSeconds(15);
                           options.ShutdownTimeout = TimeSpan.FromSeconds(30);
                       });
                       
                       services.AddTransient<PriceHistoryRepository>();
                       services.AddTransient<SubscriptionRepository>();
                       services.AddTransient<AbstractValidator<PriceHistory>, PriceHistoryValidator>();
                       services.AddTransient<AbstractValidator<TrackItem>, SubscriptionValidator>();
                       services.AddTransient<IValidation<PriceHistory>, Validation<PriceHistory>>();
                       services.AddTransient<IValidation<TrackItem>, Validation<TrackItem>>();
                       services.AddTransient<Producer<DiscountMessage>>();
                       services.AddTransient<Consumer>();
                       services.AddTransient<TrackService>();
                       services.AddTransient<MonitorStoreService>();
                       services.AddTransient<IStoreParser, MicrosoftParser>();

                       services.AddHostedService<RecurringJobsService>();
                   });
        }
    }
}
