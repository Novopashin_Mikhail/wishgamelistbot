﻿using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace PlaystationStore.Extensions
{
    public static class ServiceColletionExtensions
    {
        public static IServiceCollection AddHangfireServices(this IServiceCollection services, HostBuilderContext context)
        {
            services.TryAddSingleton(new SqlServerStorageOptions
            {
                UseRecommendedIsolationLevel = true,
            });
            services.AddHangfire((provider, configuration) => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseSqlServerStorage(context.Configuration["ConnectionString"], provider.GetRequiredService<SqlServerStorageOptions>())
            );
            services.AddHangfireServer();
            return services;
        }
    }
}
