﻿using Core;
using Hangfire;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StoreService;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PlaystationStore.HostedServices
{
    class BackgroundJobsHostedService : BackgroundService
    {
        private Setting _options;
        private IRecurringJobManager _recurringJobs;
        private ILogger<BackgroundJobsHostedService> _logger;
        private IStoreParser _parser;
        private const string TRACK_SERVICE_ID = "TrackService";
        private const string MONITOR_SERVICE_ID = "MonitorStoreService";

        public BackgroundJobsHostedService(IOptions<Setting> options, IRecurringJobManager recurringJobs, ILogger<BackgroundJobsHostedService> logger)
        {
            _options = options.Value;
            _recurringJobs = recurringJobs;
            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _recurringJobs.AddOrUpdate<ITrackService>(TRACK_SERVICE_ID, service => service.Execute(_options.ExchangeTrack), _options.CronTrackService);
                _recurringJobs.AddOrUpdate<IMonitorStoreService>(MONITOR_SERVICE_ID, service => service.Execute(), _options.CronMonitorStoreService);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            stoppingToken.Register(() =>
            {
                _recurringJobs.RemoveIfExists(TRACK_SERVICE_ID);
                _recurringJobs.RemoveIfExists(MONITOR_SERVICE_ID);
            });

            return Task.CompletedTask;
        }
    }
}
