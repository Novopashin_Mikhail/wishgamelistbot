﻿using AngleSharp;
using Core;
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PlaystationStore.Parsers
{
    class PlaystationStoreParser : IStoreParser
    {
        private IBrowsingContext _angleSharpBrowsingContext;
        private const string FIRST_WORD_REGEX = @"^\w+";
        private const string LAST_NUMBER_REGEX = @"\d+.?\d+$";

        public PlaystationStoreParser(IBrowsingContext angleSharpBrowsingContext)
        {
            _angleSharpBrowsingContext = angleSharpBrowsingContext;
        }

        public async Task<PriceHistory> GetPriceHistory(string url)
        {
            var document = await _angleSharpBrowsingContext.OpenAsync(url);

            var currentPrice = document.QuerySelector(".sku-info .price-display__price")?.TextContent?.Trim();

            if (string.IsNullOrEmpty(currentPrice))
                return null;

            var fullPrice = document.QuerySelector(".sku-info .price-display__strikethrough")?.TextContent?.Trim() ?? currentPrice;
            var title = document.QuerySelector(".pdp__title")?.TextContent;

            return new PriceHistory()
            {
                DateTime = DateTime.Now,
                NameGame = title,
                FirstPrice = GetPrice(fullPrice),
                LastPrice = GetPrice(currentPrice),
                Currency = GetCurrency(fullPrice)
            };

        }

        private decimal GetPrice(string price) => 
            decimal.TryParse(Regex.Match(price, LAST_NUMBER_REGEX).Value, NumberStyles.Any, CultureInfo.InvariantCulture, out var result) ? result : 0;

        private int GetCurrency(string price)
        {
            var currency = Regex.Match(price, FIRST_WORD_REGEX).Value;
            return currency switch
            {
                "USD" => 1,
                _ => 0
            };
        }
    }
}
