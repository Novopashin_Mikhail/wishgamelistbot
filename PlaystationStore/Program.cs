﻿using AngleSharp;
using Core;
using Core.Models.RabbitMQ;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Configuration;
using PlaystationStore.Extensions;
using PlaystationStore.Parsers;
using PlaystationStore.HostedServices;
using StoreService;

namespace PlaystationStore
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                    .ConfigureAppConfiguration((context, config) =>
                    {
                        config.AddJsonFile("appsettings.json", optional: false);
                        config.AddJsonFile("appsettings.Development.json", optional: false);
                    })
                    .ConfigureLogging((context, builder) =>
                    {
                        builder.AddConfiguration(context.Configuration.GetSection("Logging"));
                        builder.AddConsole();
                    })
                    .ConfigureServices((context, services) =>
                    {
                    
                        services.Configure<Setting>(context.Configuration);
                        services.AddDbContext<DataContext>(options =>
                        {
                            options.UseSqlServer(context.Configuration["ConnectionString"]);
                        });
                        services.AddHangfireServices(context);
                        services.AddTransient<PriceHistoryRepository>();
                        services.AddTransient<SubscriptionRepository>();
                        services.AddTransient<AbstractValidator<PriceHistory>, PriceHistoryValidator>();
                        services.AddTransient<AbstractValidator<TrackItem>, SubscriptionValidator>();
                        services.AddTransient<IValidation<PriceHistory>, Validation<PriceHistory>>();
                        services.AddTransient<IValidation<TrackItem>, Validation<TrackItem>>();
                        services.AddTransient<Producer<DiscountMessage>>();
                        services.AddTransient<Consumer>();
                        services.AddTransient<TrackService>();
                        services.AddTransient<ITrackService, TrackService>();
                        services.AddTransient<MonitorStoreService>();
                        services.AddTransient<IMonitorStoreService ,MonitorStoreService>();
                        services.AddTransient<IBrowsingContext>(_ => BrowsingContext.New(Configuration.Default.WithDefaultLoader()));

                        services.AddTransient<IStoreParser, PlaystationStoreParser>();

                        services.AddHostedService<BackgroundJobsHostedService>();
                    });
    }
}
