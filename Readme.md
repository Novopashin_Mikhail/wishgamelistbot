http://t.me/WishGameListBot - наш бот 
1. при входе пользователю выдается сообщение о том что умеет бот.
	Когда пользователь пишет старт, СhatId добавляется в таблицу чатов где 2 колонки Id, ChatId 
2. 4 кнопки дообавить, удалить, показать игры, настройки 
3. реализация работы с бд через паттерн repository 
4. добавление игры, если все данные валидны, корректная ссылка, то 
	a. добавляется запись в таблицу связи игр и телеграм-чатов там 3 колонки Id, ChatId, Guid
		таблица с играми 4 колонки Guid, Name, Url, Price
	б. отправляется request в rabbit 
5. удаление игры: 
	a. удаляется из таблицы с играми 
	б. отправялется request в rabbit
6. запрос на получение списка, по текущему ChatId получаем список игр из таблицы с играми
7. подписка на rabbit, из него получаем Guid игры и новую цену, обновляем цену в бд, далее находим связанные ChatId и отправляем рассылку с новой ценой игры


WebAPI:
1. Произведен рефакторинг логики бота, общие части вынесены в абстрактный класс.
2. Сама логика вынесена в проект-библиотеку классов.
2. Добавлен параметр в конфигурационныые файлы: *ExternalURL* - отвечает за адрес на который будет повешен телеграммовский веб-хук.
3. Для работы регистрируемся и качаем ngrok.exe и запускаем так: *ngrok http http://localhost:5000* Далее полученный URL вставляем в конфиг.

Изменения логики:
1. Плохая идея создать словарь состояний с запомненной командой, и ждать, когда вызовут команду. В asp.net контексты меняются очень стремительно. Поэтому в боте заменил словарь **Dictionary<long, ICommand> _dictState** на **Dictionary<long, string> _dictState** и создаю команду во время обращения.
2. Добавил методы установки/удаления веб-хука для бота. Внес в логику конструкторов.
3. Общую часть вынес в класс BotCore