﻿using Microsoft.EntityFrameworkCore;
using Core;

namespace StoreService
{
    public class DataContext : DbContext
    {    
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
        public DbSet<PriceHistory> PriceHistories { get; set; }
        public DbSet<TrackItem> Subscriptions { get; set; }
    }
}