using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Core;

namespace StoreService
{
    public class PriceHistoryRepository
    {
        private readonly DataContext _dataContext;
        private readonly IValidation<PriceHistory> _validation;
        private readonly ILogger<string> _logger;
        public PriceHistoryRepository(DataContext dataContext, IValidation<PriceHistory> validation, ILogger<string> logger)
        {
            _dataContext = dataContext;
            _validation = validation;
            _logger = logger;
        }

        public async Task<List<PriceHistory>> GetPirces()
        {
            try
            {
                return await _dataContext.PriceHistories.ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public async Task AddPirce(PriceHistory pirceHistory)
        {
            if (!_validation.IsValidModel(pirceHistory))
            {
                return;
            }

            try
            {
                await _dataContext.PriceHistories.AddAsync(pirceHistory);
                await _dataContext.SaveChangesAsync();

                _logger.LogInformation("Price add success!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}