using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Core;

namespace StoreService
{
    public class SubscriptionRepository
    {
        private readonly DataContext _dataContext;
        private readonly IValidation<TrackItem> _validation;
        private readonly ILogger<string> _logger;
        public SubscriptionRepository(DataContext dataContext, IValidation<TrackItem> validation, ILogger<string> logger)
        {
            _dataContext = dataContext;
            _validation = validation;
            _logger = logger;
        }

        public async Task<List<TrackItem>> GetSubscriptions()
        {
            try
            {
                return await _dataContext.Subscriptions.ToListAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }

        public void AddSubscription(TrackItem subscription)
        {
            if (!_validation.IsValidModel(subscription))
            {
                return;
            }

            try
            {
                _dataContext.Subscriptions.Add(subscription);
                _dataContext.SaveChanges();

                _logger.LogInformation("Subscription add success!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }


        }

        public async Task UpdateSubscription(TrackItem subscription)
        {
            if (!_validation.IsValidModel(subscription))
            {
                return;
            }

            try
            {
                _dataContext.Subscriptions.Update(subscription);
                await _dataContext.SaveChangesAsync();

                _logger.LogInformation("Subscription update success!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        public void DeleteSubscription(TrackItem subscription)
        {
            if (!_validation.IsValidModel(subscription))
            {
                return;
            }

            try
            {
                var subscriptionFromBD = _dataContext.Subscriptions.FirstOrDefault(f => f.Url == subscription.Url &&
                                                                                      f.NameGame == subscription.NameGame &&
                                                                                      f.IdUser == subscription.IdUser);
                if (subscriptionFromBD != null)
                {
                    _dataContext.Subscriptions.Remove(subscriptionFromBD);
                    _dataContext.SaveChanges();
                }

                _logger.LogInformation("Subscription delete success!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

    }
}