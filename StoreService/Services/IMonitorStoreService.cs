﻿using System.Threading.Tasks;

namespace StoreService
{
    public interface IMonitorStoreService
    {
        Task Execute();
    }
}