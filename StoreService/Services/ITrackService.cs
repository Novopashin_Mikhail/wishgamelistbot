﻿using System.Threading.Tasks;

namespace Core
{
    public interface ITrackService
    {
        Task Execute(string exchangeName);
    }
}