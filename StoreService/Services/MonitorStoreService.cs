﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Core;
using Core.Models;
using Core.Models.RabbitMQ;

namespace StoreService
{
    public class MonitorStoreService : IMonitorStoreService
    {
        private readonly Setting _setting;
        private readonly SubscriptionRepository _subscriptionRepository;
        private readonly PriceHistoryRepository _priceHistoryRepository;
        private readonly Producer<DiscountMessage> _producer;
        private readonly IStoreParser _storeParser;
        private readonly ILogger<string> _logger;
        public MonitorStoreService(IOptions<Setting> options, SubscriptionRepository subscriptionRepository, PriceHistoryRepository priceHistoryRepository,
                                    Producer<DiscountMessage> producer, IStoreParser storeParser, ILogger<string> logger)
        {
            _setting = options.Value;
            _subscriptionRepository = subscriptionRepository;
            _priceHistoryRepository = priceHistoryRepository;
            _producer = producer;
            _storeParser = storeParser;
            _logger = logger;
        }
        public async Task Execute()
        {
            _logger.LogInformation("Start MonitorStoreService...");

            var subscriptions = await _subscriptionRepository.GetSubscriptions();
            if (subscriptions != null)
            {
                foreach (var groupSubscription in subscriptions.GroupBy(g => g.Url))
                {
                    var priceHistory = await _storeParser.GetPriceHistory(groupSubscription.Key);
                    if (priceHistory != null)
                    {
                        await _priceHistoryRepository.AddPirce(priceHistory);

                        foreach (var subscription in subscriptions.Where(w => w.Url == groupSubscription.Key))
                        {
                            if (priceHistory.LastPrice == subscription.LastPrice)
                                continue;

                            subscription.FirstPrice = priceHistory.FirstPrice;
                            subscription.LastPrice = priceHistory.LastPrice;
                            await _subscriptionRepository.UpdateSubscription(subscription);

                            if (priceHistory.LastPrice < subscription.LastPrice)
                                await _producer.Send(_setting.ExchangePrice, new DiscountMessage()
                                {
                                    ItemGuid = subscription.IdUser,
                                    ItemName = subscription.NameGame,
                                    ItemLink = subscription.Url,
                                    FullPrice = priceHistory.FirstPrice,
                                    DiscountPrice = priceHistory.LastPrice,
                                    Currency = (Currency)priceHistory.Currency
                                });
                        }
                    }
                }
            }

            _logger.LogInformation("Stop MonitorStoreService.");
        }
    }
}
