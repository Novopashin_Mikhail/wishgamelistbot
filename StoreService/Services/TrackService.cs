﻿using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Events;
using Core.Models.RabbitMQ;
using StoreService;

namespace Core
{
    public class TrackService : ITrackService
    {
        private readonly Consumer _consumer;
        private readonly SubscriptionRepository _subscriptionRepository;
        private readonly ILogger<string> _logger;
        public TrackService(Consumer consumer, SubscriptionRepository subscriptionRepository, ILogger<string> logger)
        {
            _consumer = consumer;
            _subscriptionRepository = subscriptionRepository;
            _logger = logger;
        }
        public async Task Execute(string exchangeName)
        {
            _logger.LogInformation("Start TrackService...");

            await Task.Run(() => _consumer.Receive(exchangeName, MessageReceive));

            _logger.LogInformation("Stop TrackService.");
        }

        private void MessageReceive(BasicDeliverEventArgs ea)
        {
            var track = JsonSerializer.Deserialize<TrackedItemMessage>(Encoding.UTF8.GetString(ea.Body.Span));

            _logger.LogInformation("Message receive success!");

            var subscription = new TrackItem()
            {
                IdUser = track.ItemGuid,
                NameGame = track.ItemName,
                Url = track.ItemLink
            };

            switch (track.Action)
            {
                case ItemActions.Add:
                    _subscriptionRepository.AddSubscription(subscription);
                    break;
                case ItemActions.Delete:
                    _subscriptionRepository.DeleteSubscription(subscription);
                    break;
            }
        }
    }
}
