﻿using FluentValidation;
using Core;

namespace StoreService
{
    public class PriceHistoryValidator : AbstractValidator<PriceHistory>
    {
        public PriceHistoryValidator()
        {
            RuleFor(priceHistory => priceHistory.NameGame)
                .NotNull().WithMessage("Имя игры не должно быть пустым")
                .MaximumLength(500).WithMessage("Длина имени игры не должна быть больше 500 символов");                
                        
            RuleFor(priceHistory => priceHistory.Currency)              
              .Must(currency=>currency>=0 && currency<=2).WithMessage("Значение курса валюты должно быть 0-2");
        }
    }
}
