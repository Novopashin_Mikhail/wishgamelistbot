﻿using FluentValidation;
using Core;

namespace StoreService
{
    public class SubscriptionValidator : AbstractValidator<TrackItem>
    {
        public SubscriptionValidator()
        {
            RuleFor(subscription => subscription.NameGame)
                .NotNull().WithMessage("Имя игры не должно быть пустым")
                .MaximumLength(500).WithMessage("Длина имени игры не должна быть больше 500 символов");                
                        
            RuleFor(subscription => subscription.Url)
              .NotNull().WithMessage("Ссылка не должна быть пустой")
              .Matches(@"^https?://[^\s]+$").WithMessage("Формат ссылки некорректен")
              .MaximumLength(4000).WithMessage("Длина ссылки не должна быть больше 4000 символов");

            RuleFor(subscription => subscription.IdUser)
                .NotNull().WithMessage("UUID пользователя не должен быть пустым");            
        }
    }
}
