﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using WishGameListBot.Rabbit;

namespace TGWebHookBased
{
    public class ConsumerHostedService : IHostedService
    {
        private readonly ConsumerDiscounts _rabbitConsumerDicsounts;
        private Task _rabbitConsumerDiscountsTask;
        private CancellationTokenSource CancellationTokenSource { get; } = new CancellationTokenSource();
        
        public ConsumerHostedService(ConsumerDiscounts rabbitConsumerDicsounts)
        {
            _rabbitConsumerDicsounts = rabbitConsumerDicsounts;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _rabbitConsumerDiscountsTask = Task.Run(() => _rabbitConsumerDicsounts.Run(), cancellationToken);
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_rabbitConsumerDiscountsTask == null)
            {
                return;
            }
            
            try
            {
                CancellationTokenSource.Cancel();
                _rabbitConsumerDicsounts.Dispose();
            }
            finally
            {
                await Task.WhenAny(_rabbitConsumerDiscountsTask, Task.Delay(Timeout.Infinite, cancellationToken));
            }
        }
    }
}