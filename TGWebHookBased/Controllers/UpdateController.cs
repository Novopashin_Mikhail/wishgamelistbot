﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WishGameListBot.BotWorker;
using Telegram.Bot.Types;

namespace TGWebHookBased.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateController : ControllerBase
    {
        private ITGBot _botWorker;
        public UpdateController(ITGBot bw, ILogger<UpdateController> logger)
        {
            _botWorker = bw;
            _logger = logger;
        }

        private readonly ILogger<UpdateController> _logger;

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("This is bot for update from TG");
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Update update)
        {
            await (_botWorker as WebHookBot).UpdateHandler(update);
            return Ok();
        }
    }
}