using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WishGameListBot;
using WishGameListBot.BotWorker;
using WishGameListBot.Commands;
using WishGameListBot.ReferenceValidator;
using WishGameListBot.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WishGameListBot.Rabbit;
using WishGameListBot.Services;

namespace TGWebHookBased
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.Configure<Configuration>(Configuration.GetSection("Configuration"));
            services.AddTransient<CommandsCreator>();
            services.AddTransient<ITGBot, WebHookBot>();

            services.AddControllers().AddNewtonsoftJson();

            services.AddTransient<IValidator, Validator>();
            services.AddTransient<ConsumerDiscounts>();
            services.AddTransient<INotifierService, NotifierService>();
            services.AddHostedService<ConsumerHostedService>();
            services.AddTransient<PublisherTrackedItem, PublisherTrackedItem>();

            services.AddTransient<AddCommand>()
                .AddTransient<ICommand, AddCommand>(x => x.GetService<AddCommand>());

            services.AddTransient<ShowCommand>()
                .AddTransient<ICommand, ShowCommand>(x => x.GetService<ShowCommand>());

            services.AddTransient<RemoveCommand>()
                .AddTransient<ICommand, RemoveCommand>(x => x.GetService<RemoveCommand>());

            services.AddTransient<StartCommand>()
                .AddTransient<ICommand, StartCommand>(x => x.GetService<StartCommand>());

            services.AddTransient<SettingCommand>()
                .AddTransient<ICommand, SettingCommand>(x => x.GetService<SettingCommand>());

            services.AddTransient<HelpCommand>()
                .AddTransient<ICommand, HelpCommand>(x => x.GetService<HelpCommand>());

            services.AddTransient<IRepository, Repository>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddDbContext<MsSqlDataContext>((services, options) =>
            {
                var config = services.GetService<IOptionsSnapshot<Configuration>>();
                options.UseSqlServer(config.Value.ConnectionString);
            },ServiceLifetime.Transient);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
