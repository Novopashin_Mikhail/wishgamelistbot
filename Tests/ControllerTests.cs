﻿using System;
using NUnit.Framework;
using TGWebHookBased.Controllers;
using WishGameListBot;

namespace Tests
{
    [TestFixture]
    public class ControllerTests
    {
        UpdateController _updateController;
        public ControllerTests()
        {
            _updateController = new UpdateController(null,null);
        }
        [Test]
        public void UpdateGetTest()
        {
            //Arrange 
            string warning_mes = "This is bot for update from TG";
            //Act
            var result = _updateController.Get() as Microsoft.AspNetCore.Mvc.OkObjectResult;
            //Assert
            Assert.AreEqual(result.Value, warning_mes, "Return wrong GET message");
        }
        [Test]
        public void UpdateGetStatusTest()
        {
            //Arrange
            int statusCode = 200;
            //Act
            var result = _updateController.Get() as Microsoft.AspNetCore.Mvc.OkObjectResult;
            //Assert
            Assert.AreEqual(statusCode, result.StatusCode);
        }
    }
}
